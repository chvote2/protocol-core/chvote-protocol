/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support;

import static ch.ge.ve.protocol.support.WriterUtils.FIELD_SEPARATOR;
import static ch.ge.ve.protocol.support.WriterUtils.fieldKey;

import ch.ge.ve.protocol.support.exception.VerificationDataWriterRuntimeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntegerMapWriter<T> implements Closeable {
  private static final Logger log = LoggerFactory.getLogger(IntegerMapWriter.class);

  private final ObjectMapper  objectMapper;
  private final Writer        writer;
  private final String        fieldName;
  private final List<Integer> expectedKeys;
  private final boolean       writeNextFieldSeparator;

  private final Map<Integer, T> valueMap    = new ConcurrentHashMap<>();
  private final Set<Integer>    keysWritten = new ConcurrentSkipListSet<>();
  private final Object          mutex       = new Object();

  public IntegerMapWriter(ObjectMapper objectMapper, Writer writer, String fieldName,
                          Collection<Integer> expectedKeys, boolean writeNextFieldSeparator) {
    this.objectMapper = objectMapper;
    this.writer = writer;
    this.fieldName = fieldName;
    this.expectedKeys = ImmutableList.sortedCopyOf(Comparator.naturalOrder(), expectedKeys);
    this.writeNextFieldSeparator = writeNextFieldSeparator;
  }

  public void publishValues(Map<Integer, T> values) {
    Preconditions.checkArgument(expectedKeys.containsAll(values.keySet()));
    valueMap.putAll(values);
    writeItems();
  }

  public boolean done() {
    return keysWritten.containsAll(expectedKeys);
  }

  private void writeItems() {
    while (writeNextItem()) {
      log.debug("Wrote next integer map item");
    }
  }

  private boolean writeNextItem() {
    boolean callAgain = false;
    synchronized (mutex) {
      try {
        Integer key = expectedKeys.get(keysWritten.size());
        if (valueMap.containsKey(key)) {
          if (keysWritten.isEmpty()) {
            writer.append(fieldName != null ? fieldKey(fieldName, true) : "{");
          }
          writer.append(String.format("\"%d\": ", key));
          writer.append(objectMapper.writeValueAsString(valueMap.remove(key)));
          keysWritten.add(key);
          if (done()) {
            writeEnd();
          } else {
            writer.append(FIELD_SEPARATOR);
            callAgain = true;
          }
        }
      } catch (IOException e) {
        log.error("Failed to write public credentials entry");
        throw new VerificationDataWriterRuntimeException(e);
      }
    }

    return callAgain;
  }

  private void writeEnd() throws IOException {
    writer.append(String.format("%n}"));
    if (writeNextFieldSeparator) {
      writer.append(String.format(",%n"));
    }
  }

  @Override
  public void close() throws IOException {
    writer.close();
  }
}

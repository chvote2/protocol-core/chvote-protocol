/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.BuildPublicCredentialsRequestEvent;
import ch.ge.ve.event.ControlComponentInitializedPublicationEvent;
import ch.ge.ve.event.ControlComponentPublicKeyMulticastEvent;
import ch.ge.ve.event.ControlComponentPublicKeyPublicationEvent;
import ch.ge.ve.event.ElectionOfficerPublicKeyMulticastEvent;
import ch.ge.ve.event.ElectionSetMulticastEvent;
import ch.ge.ve.event.KeyGenerationRequestEvent;
import ch.ge.ve.event.PrimesPublicationEvent;
import ch.ge.ve.event.PublicCredentialsPartMulticastEvent;
import ch.ge.ve.event.PublicCredentialsPartPublicationEvent;
import ch.ge.ve.event.PublicParametersMulticastEvent;
import ch.ge.ve.event.SignEncryptedPrivateCredentialsRequestEvent;
import ch.ge.ve.event.SignEncryptedPrivateCredentialsResponseEvent;
import ch.ge.ve.event.VotersMulticastEvent;
import ch.ge.ve.event.VotersPublishedEvent;
import ch.ge.ve.event.payload.ControlComponentInitializedPayload;
import ch.ge.ve.event.payload.ControlComponentPublicKeyPayload;
import ch.ge.ve.event.payload.PrimesPayload;
import ch.ge.ve.event.payload.PublicCredentialsPartPayload;
import ch.ge.ve.event.payload.VoterIdsPayload;
import ch.ge.ve.protocol.controlcomponent.ControlComponent;
import ch.ge.ve.protocol.controlcomponent.exception.SignEncryptionException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.VoterData;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Signature;
import ch.ge.ve.service.AbstractAcknowledgeableSignedEventsListener;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import ch.ge.ve.service.SignatureService;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Control component initialization events listener.
 */
@Component
@RabbitListener(
    bindings = @QueueBinding(
        value = @Queue(
            value = Channels.CONTROL_COMPONENT_INITIALIZATION + "-controlComponent-${ch.ge.ve.cc.index}",
            arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
            durable = "true"),
        exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                             value = Channels.CONTROL_COMPONENT_INITIALIZATION)
    )
)
public class ControlComponentInitializationListener extends AbstractAcknowledgeableSignedEventsListener {
  private static final Logger logger = LoggerFactory.getLogger(ControlComponentInitializationListener.class);

  private final ControlComponent controlComponent;
  private final int              ccIndex;
  private final String           loopbackExchange;

  @Autowired
  public ControlComponentInitializationListener(ControlComponent controlComponent, EventBus eventBus,
                                                SignatureService signatureService) {
    super(eventBus, Endpoints.CONTROL_COMPONENT + "-" + controlComponent.getIndex(), signatureService);
    this.controlComponent = controlComponent;
    this.ccIndex = controlComponent.getIndex();
    this.loopbackExchange = Channels.CONTROL_COMPONENT_BUILD_CREDS + "-" + endpoint;
  }

  @RabbitHandler
  public void processPublicParametersMulticastEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                    PublicParametersMulticastEvent event) {
    PublicParameters publicParameters = event.getPayload().getPublicParameters();
    publicParameters.runIntensivePreconditions();
    logger.info("Received public params: {}", publicParameters);
    controlComponent.storePublicParameters(protocolId, publicParameters);
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processKeyGenerationRequestEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                               KeyGenerationRequestEvent event) {
    logger.info("Received key generation request, generating keys");
    EncryptionPublicKey publicKey = controlComponent.generateKeys(protocolId);
    logger.info("Generated encryption keys, publishing public key part");
    ControlComponentPublicKeyPayload payload = new ControlComponentPublicKeyPayload(ccIndex, publicKey);
    Signature signature = sign(protocolId, payload);
    eventBus.publish(Channels.BULLETIN_BOARD_INITIALIZATION, protocolId, endpoint,
                     new ControlComponentPublicKeyPublicationEvent(endpoint, payload, signature, verificationKeyHash));
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processControlComponentPublicKeyMulticastEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                             ControlComponentPublicKeyMulticastEvent event) {
    int controlComponentIndex = event.getPayload().getControlComponentIndex();
    logger.info("Received control component #{}'s public key", controlComponentIndex);
    controlComponent.storeKeyPart(protocolId, controlComponentIndex, event.getPayload().getEncryptionPublicKey());
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processElectionOfficerPublicKeyMulticastEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                            ElectionOfficerPublicKeyMulticastEvent event) {
    logger.info("Received election officer public key");
    EncryptionPublicKey encryptionPublicKey = event.getPayload().getEncryptionPublicKey();
    controlComponent.storeElectionOfficerPublicKey(protocolId, encryptionPublicKey).ifPresent(key -> {
      ControlComponentInitializedPayload payload = new ControlComponentInitializedPayload(ccIndex);
      eventBus.publish(Channels.CONTROL_COMPONENT_INITIALIZED, protocolId, endpoint,
                       new ControlComponentInitializedPublicationEvent(endpoint, payload));
    });
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processElectionSetMulticastEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                               ElectionSetMulticastEvent event) throws NotEnoughPrimesInGroupException {
    controlComponent.storeElectionSet(protocolId, event.getPayload().getElectionSet());
    logger.info("Election set received; we may now generate the list of primes to use for encoding candidates");
    List<BigInteger> primes = controlComponent.generatePrimes(protocolId);
    eventBus.publish(Channels.BULLETIN_BOARD_INITIALIZATION, protocolId, endpoint,
                     new PrimesPublicationEvent(endpoint, new PrimesPayload(primes)));
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processVotersMulticastEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                          VotersMulticastEvent event) {
    logger.info("Received voters, generating voter data");
    List<VoterData> electorateData = controlComponent.generateVoterData(protocolId, event.getPayload().getVoters());
    logger.info("Generated electorate data, publishing public credentials part");
    Map<Integer, Point> publicVoterDataMap =
        electorateData.stream().collect(Collectors.toMap(VoterData::getVoterId, VoterData::getPublicVoterData));
    publishPublicCredentialsPart(protocolId, publicVoterDataMap);
    acknowledge(protocolId, event);
  }

  private void publishPublicCredentialsPart(String protocolId, Map<Integer, Point> publicVoterData) {
    PublicCredentialsPartPayload payload = new PublicCredentialsPartPayload(ccIndex, publicVoterData);
    Signature signature = sign(protocolId, payload);
    eventBus.publish(Channels.BULLETIN_BOARD_INITIALIZATION, protocolId, endpoint,
                     new PublicCredentialsPartPublicationEvent(endpoint, payload, signature, verificationKeyHash));
  }

  @RabbitHandler
  public void processPublicCredentialsPartMulticastEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                         PublicCredentialsPartMulticastEvent event) {
    PublicCredentialsPartPayload payload = event.getPayload();
    int controlComponentIndex = payload.getControlComponentIndex();
    logger.info("Received public credentials part from control component #{}", controlComponentIndex);
    controlComponent.storePublicCredentialsPart(protocolId, controlComponentIndex, payload.getPublicCredentialsPart());
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processVotersPublishedEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                          VotersPublishedEvent event) {
    if (controlComponent.hasReceivedAllPublicCredentialParts(protocolId)) {
      List<List<Integer>> voterIdBatches = controlComponent.getVoterIdBatches(protocolId);
      final int sum = voterIdBatches.stream().mapToInt(List::size).sum();
      logger.info("Found {} voter ids across {} messages", sum, voterIdBatches.size());
      controlComponent.forceEventLogIndex();
      voterIdBatches.forEach(
          batch -> eventBus.publish(loopbackExchange, protocolId, endpoint,
                                    new BuildPublicCredentialsRequestEvent(endpoint, new VoterIdsPayload(batch)))
      );
      acknowledge(protocolId, event);
    } else {
      logger.info("I'll retry later, thanks!");
      throw new IllegalStateException("not ready");
    }
  }

  @RabbitHandler
  public void processSignEncryptedPrivateCredentialsRequestEvent(
      @Header(EventHeaders.PROTOCOL_ID) String protocolId, SignEncryptedPrivateCredentialsRequestEvent event)
      throws SignEncryptionException {
    logger.info("Received private credentials request...");
    final List<Integer> voterIds = event.getPayload().getVoterIds();
    Map<PrintingAuthorityWithPublicKey, byte[]> credentials =
        controlComponent.getSignEncryptedPrivateCredentials(protocolId, voterIds);
    final int min = voterIds.stream().mapToInt(Integer::intValue).min().orElse(0);
    final int max = voterIds.stream().mapToInt(Integer::intValue).max().orElse(Integer.MAX_VALUE);
    eventBus.publish(Channels.SIGN_ENCRYPTED_PRIVATE_CREDENTIALS, protocolId, endpoint,
                     new SignEncryptedPrivateCredentialsResponseEvent(event.getEventId(), endpoint, ccIndex,
                                                                      credentials, String.format("%d-%d", min, max)));
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.acknowledgement.repository.AcknowledgementPendingEventJpaRepository;
import ch.ge.ve.protocol.controlcomponent.ControlComponent;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.service.AcknowledgementService;
import ch.ge.ve.service.AcknowledgementServiceJpaImpl;
import ch.ge.ve.service.Signatories;
import ch.ge.ve.service.SignatureService;
import ch.ge.ve.service.SignatureServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.security.Security;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ControlComponentMessagingApplication {

  public static void main(String[] args) {
    SpringApplication.run(ControlComponentMessagingApplication.class, args);
  }

  @Bean
  public SignatureService signatureService(@Value("${ch.ge.ve.cc.index}") int index,
                                           IdentificationPrivateKey signingKey,
                                           Map<String, Map<String, BigInteger>> verificationKeys,
                                           AlgorithmsSpec algorithmsSpec,
                                           RandomGenerator randomGenerator,
                                           ObjectMapper objectMapper,
                                           ControlComponent controlComponent) {
    return new SignatureServiceImpl(Signatories.buildControlComponentSignatory(index), signingKey, verificationKeys,
                                    algorithmsSpec, randomGenerator, objectMapper,
                                    controlComponent::getPublicParameters);
  }

  @Bean
  public AcknowledgementService acknowledgementService(ObjectMapper mapper,
                                                       AcknowledgementPendingEventJpaRepository repository) {
    return new AcknowledgementServiceJpaImpl(repository, mapper);
  }

  @Bean
  public IdentificationPrivateKey controlComponentSigningKey(@Value("${ch.ge.ve.cc.signing-key.url}") URL signingKeyUrl,
                                                             @Value("${ch.ge.ve.cc.signing-key.password}") char[] signingKeyPassword,
                                                             ObjectMapper objectMapper) {
    // TODO: protect key with a password
    try {
      return objectMapper.readValue(signingKeyUrl, IdentificationPrivateKey.class);
    } catch (IOException e) {
      throw new IllegalStateException("Initialization failure: can't read signing key", e);
    }
  }

  @Bean
  public RandomGenerator randomGenerator(AlgorithmsSpec algorithmsSpec) {
    return RandomGenerator.create(algorithmsSpec.getRandomGeneratorAlgorithm(),
                                  algorithmsSpec.getRandomGeneratorProvider());
  }

  @PostConstruct
  public void addBouncyCastleProvider() {
    Security.addProvider(new BouncyCastleProvider());
  }
}

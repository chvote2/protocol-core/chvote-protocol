/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.BuildPublicCredentialsFinishedEvent;
import ch.ge.ve.event.BuildPublicCredentialsRequestEvent;
import ch.ge.ve.protocol.controlcomponent.ControlComponent;
import ch.ge.ve.service.AbstractAcknowledgeableEventsListener;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Control component build public credentials request events listener.
 */
@Component
@RabbitListener(
    bindings = @QueueBinding(
        value = @Queue(
            value = Channels.CONTROL_COMPONENT_BUILD_CREDS + "-controlComponent-${ch.ge.ve.cc.index}",
            arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
            durable = "true"),
        exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                             value = Channels.CONTROL_COMPONENT_BUILD_CREDS + "-controlComponent-${ch.ge.ve.cc.index}")
    )
)
public class ControlComponentBuildCredentialsListener extends AbstractAcknowledgeableEventsListener {
  private static final Logger log = LoggerFactory.getLogger(ControlComponentInitializationListener.class);

  private final ControlComponent controlComponent;

  @Autowired
  public ControlComponentBuildCredentialsListener(ControlComponent controlComponent, EventBus eventBus) {
    super(eventBus, Endpoints.CONTROL_COMPONENT + "-" + controlComponent.getIndex());
    this.controlComponent = controlComponent;
  }

  @RabbitHandler
  public void processBuildPublicCredentialsRequestEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                        BuildPublicCredentialsRequestEvent event) {
    List<Integer> voterIds = event.getPayload().getVoterIds();
    log.info("Received build public credentials request event for {} voterIds", voterIds.size());
    controlComponent.buildPublicCredentials(protocolId, voterIds);
    if (controlComponent.doneBuildingPublicCredentials(protocolId)) {
      eventBus.publish(Channels.CONTROL_COMPONENT_BUILD_CREDS, protocolId,
                       String.format("%s-%d", Endpoints.CONTROL_COMPONENT, controlComponent.getIndex()),
                       new BuildPublicCredentialsFinishedEvent(controlComponent.getIndex()));
    }
    acknowledge(protocolId, event);
  }
}

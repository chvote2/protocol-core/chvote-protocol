/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.PreDestroy;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.core.config.DefaultConfiguration;
import org.ehcache.impl.config.persistence.DefaultPersistenceConfiguration;
import org.ehcache.impl.copy.IdentityCopier;
import org.ehcache.jsr107.Eh107Configuration;
import org.ehcache.jsr107.EhcacheCachingProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CachingConfiguration {

  private static final Logger logger                     = LoggerFactory.getLogger(CachingConfiguration.class);
  private static final int    MAX_OBJECTS_COUNT_IN_GRAPH = 1_000_000;

  private final CachingProvider provider = Caching.getCachingProvider("org.ehcache.jsr107.EhcacheCachingProvider");

  @Bean
  public JCacheCacheManager cacheManager(@Value("${ch.ge.ve.cache.tiers.heap.size}") long heapTierSize,
                                         @Value("${ch.ge.ve.cache.tiers.off-heap.size}") long offHeapTierSize,
                                         @Value("${ch.ge.ve.cache.tiers.disk.size}") long diskTierSize,
                                         @Value("${ch.ge.ve.cache.tiers.disk.directory}") File rootDirectory) {
    if (diskTierSize > 0L && rootDirectory == null) {
      throw new IllegalArgumentException("Root directory for disk tier must be specified!");
    }
    ResourcePoolsBuilder resources = createResourcePoolsBuilder(heapTierSize, offHeapTierSize, diskTierSize);
    IdentityCopier<Object> identityCopier = new IdentityCopier<>();
    CacheConfiguration defaultCacheConfig =
        CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class, resources)
                                 .withSizeOfMaxObjectGraph(MAX_OBJECTS_COUNT_IN_GRAPH)
                                 .withKeyCopier(identityCopier)
                                 .withValueCopier(identityCopier)
                                 .build();
    CacheManager cacheManager = getCacheManager(rootDirectory);
    cacheManager.createCache("default", Eh107Configuration.fromEhcacheCacheConfiguration(defaultCacheConfig));
    return new JCacheCacheManager(cacheManager);
  }

  private CacheManager getCacheManager(File diskTierRootDirectory) {
    EhcacheCachingProvider ehcache = (EhcacheCachingProvider) provider;
    DefaultConfiguration cacheManagerConfig;
    if (diskTierRootDirectory != null) {
      logger.info("Disk tier root directory: {}", diskTierRootDirectory);
      DefaultPersistenceConfiguration persistenceConfig = new DefaultPersistenceConfiguration(diskTierRootDirectory);
      cacheManagerConfig = new DefaultConfiguration(ehcache.getDefaultClassLoader(), persistenceConfig);
    } else {
      cacheManagerConfig = new DefaultConfiguration(ehcache.getDefaultClassLoader());
    }
    return ehcache.getCacheManager(ehcache.getDefaultURI(), cacheManagerConfig);
  }

  private ResourcePoolsBuilder createResourcePoolsBuilder(long heapTierSize, long offHeapTierSize, long diskTierSize) {
    logger.info("Heap tier size: {} MiB", heapTierSize);
    ResourcePoolsBuilder builder = ResourcePoolsBuilder.newResourcePoolsBuilder().heap(heapTierSize, MemoryUnit.MB);
    if (offHeapTierSize > 0L) {
      logger.info("Off-heap tier size: {} MiB", offHeapTierSize);
      builder = builder.offheap(offHeapTierSize, MemoryUnit.MB);
    }
    if (diskTierSize > 0L) {
      logger.info("Disk tier size: {} MiB", diskTierSize);
      builder = builder.disk(diskTierSize, MemoryUnit.MB);
    }
    return builder;
  }

  @Bean
  public KeyGenerator customKeyGenerator() {
    return CustomCacheKey::new;
  }

  @PreDestroy
  public void disposeCaches() {
    provider.close();
  }

  private static final class CustomCacheKey implements Serializable {
    private final String             target;
    private final List<Serializable> args;
    private final int                hashCode;

    CustomCacheKey(Object o, Method m, Object... args) {
      this.target = o.getClass().getName() + "#" + m.getName();
      this.args = Arrays.stream(args)
                        .map(arg -> {
                          if (!(arg instanceof Serializable)) {
                            throw new IllegalArgumentException(
                                String.format("Method %s can't be invoked with non serializable argument", target));
                          }
                          return (Serializable) arg;
                        })
                        .collect(Collectors.toList());
      this.hashCode = Objects.hash(target);
    }

    @Override
    public int hashCode() {
      return hashCode;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      CustomCacheKey k = (CustomCacheKey) o;
      return Objects.equals(args, k.args) && Objects.equals(target, k.target);
    }
  }
}

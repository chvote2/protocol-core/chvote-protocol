/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation.model;

import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.Voter;
import java.util.List;

/**
 * This class is only used in a simulation / test setting,
 * to generate an election set along with emulated, compatible voters
 */
public class ElectionSetAndVoters {
  private final ElectionSetWithPublicKey electionSet;
  private final List<Voter>              voters;

  public ElectionSetAndVoters(ElectionSetWithPublicKey electionSet, List<Voter> voters) {
    this.electionSet = electionSet;
    this.voters = voters;
  }

  public ElectionSetWithPublicKey getElectionSet() {
    return electionSet;
  }

  public List<Voter> getVoters() {
    return voters;
  }
}


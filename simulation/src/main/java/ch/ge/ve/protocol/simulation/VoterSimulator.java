/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.protocol.core.algorithm.VoteConfirmationVoterAlgorithms;
import ch.ge.ve.protocol.core.model.VotingCard;
import ch.ge.ve.protocol.simulation.exception.FinalizationCodeNotMatchingException;
import ch.ge.ve.protocol.simulation.exception.SimulationException;
import ch.ge.ve.protocol.simulation.exception.VerificationCodesNotMatchingException;
import ch.ge.ve.protocol.simulation.exception.VoteConfirmationException;
import ch.ge.ve.protocol.simulation.exception.VoteProcessException;
import ch.ge.ve.protocol.support.model.VotingPageData;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simulation class for a Voter, with interfaces to receive the code sheet, initiate a voting client, and perform
 * selections on elections.
 */
final class VoterSimulator {
  private static final Logger log = LoggerFactory.getLogger(VoterSimulator.class);

  private final Integer                         voterIndex;
  private final VotingClient                    votingClient;
  private final VoteConfirmationVoterAlgorithms voteConfirmationVoterAlgorithms;

  // Not specifying algorithm / provider, since this doesn't need to
  // be a CSPRNG, it is only used to simulate voter choices
  private final Random random = new SecureRandom();

  private VotingCard votingCard;

  VoterSimulator(Integer voterIndex, VotingClient votingClient,
                 VoteConfirmationVoterAlgorithms voteConfirmationVoterAlgorithms) {
    this.votingClient = votingClient;
    this.voterIndex = voterIndex;
    this.voteConfirmationVoterAlgorithms = voteConfirmationVoterAlgorithms;
  }

  void sendCodeSheet(VotingCard votingCard) {
    Preconditions.checkState(this.votingCard == null,
                             "The code sheet may not be updated once set (at voter %s)", voterIndex);
    Preconditions
        .checkArgument(votingCard.getVoter().getVoterId() == voterIndex, "Voter received the wrong code lists");
    this.votingCard = votingCard;
    List<String> bold_rc = votingCard.getBold_rc();
    long nonNullCount = bold_rc.stream().filter(Predicates.not(String::isEmpty)).count();
    log.info("Voter {} received {} return codes ({} not empty)", voterIndex, bold_rc.size(), nonNullCount);
  }

  List<Integer> vote() {
    Preconditions.checkState(votingCard != null, "The voter needs their code sheet to vote");

    log.info("Voter {} starting vote", voterIndex);
    VotingPageData votingPageData = votingClient.startVoteSession(voterIndex);

    List<Integer> selections = pickAtRandom(votingPageData.getSelectionCounts(), votingPageData.getCandidateCounts());
    log.info("Voter {} selections: {}", voterIndex, selections);

    List<String> verificationCodes;
    try {
      log.info("Voter {} submitting vote", voterIndex);
      verificationCodes = votingClient.sumbitVote(votingCard.getUpper_x(), selections);
    } catch (SimulationException e) {
      log.error(String.format("Voter %d: error during vote casting", voterIndex), e);
      throw new VoteProcessException(e);
    }

    log.info("Voter {} checking verification codes", voterIndex);
    log.info("Voter {}, verification codes: {}, received codes: {}, selections: {}", voterIndex,
             votingCard.getBold_rc(), verificationCodes,
             selections);
    if (!voteConfirmationVoterAlgorithms.checkReturnCodes(votingCard.getBold_rc(), verificationCodes, selections)) {
      throw new VoteProcessException(new VerificationCodesNotMatchingException("Verification codes do not match"));
    }

    String finalizationCode;
    try {
      log.info("Voter {} confirming vote", voterIndex);
      finalizationCode = votingClient.confirmVote(votingCard.getUpper_y());
    } catch (VoteConfirmationException e) {
      log.error(String.format("Voter %d: error during vote confirmation", voterIndex), e);
      throw new VoteProcessException(e);
    }

    log.info("Voter {} checking finalization code", voterIndex);
    if (!voteConfirmationVoterAlgorithms.checkFinalizationCode(votingCard.getUpper_fc(), finalizationCode)) {
      throw new VoteProcessException(new FinalizationCodeNotMatchingException("Finalization code does not match"));
    }

    log.info("Voter {} done voting", voterIndex);

    return selections;
  }

  private List<Integer> pickAtRandom(List<Integer> selectionCounts, List<Integer> candidateCounts) {
    Preconditions.checkArgument(selectionCounts.size() == candidateCounts.size(),
                                "The size of both lists should be identical");

    List<Integer> selections = new ArrayList<>();
    int n_offset = 1;

    for (int i = 0; i < selectionCounts.size(); i++) {
      int numberOfSelections = selectionCounts.get(i);
      int numberOfCandidates = candidateCounts.get(i);

      for (int j = 0; j < numberOfSelections; j++) {
        Integer s_j;
        do {
          s_j = random.nextInt(numberOfCandidates) + n_offset;
        } while (selections.contains(s_j));
        selections.add(s_j);
      }

      n_offset += numberOfCandidates;
    }

    return selections.stream().sorted().collect(Collectors.toList());
  }

  VotingClient getVotingClient() {
    return votingClient;
  }

  Integer getVoterIndex() {
    return voterIndex;
  }
}

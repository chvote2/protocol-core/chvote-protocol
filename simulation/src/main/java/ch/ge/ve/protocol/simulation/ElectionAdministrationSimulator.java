/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.protocol.bulletinboard.model.TallyData;
import ch.ge.ve.protocol.core.algorithm.DecryptionAuthorityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.GeneralAlgorithms;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.algorithm.TallyingAuthoritiesAlgorithm;
import ch.ge.ve.protocol.core.exception.EncryptionColouringRuntimeException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.support.MoreCollectors;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.simulation.exception.InvalidDecryptionProofException;
import ch.ge.ve.protocol.simulation.model.TallyDecryptionAndProof;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class simulates the actions of the election administration.
 */
final class ElectionAdministrationSimulator {
  private static final String COLOUR_EXCEPTION = "Failed to retrieve color from ballot";
  private static final Logger perfLog          = LoggerFactory.getLogger("PerformanceStats");

  private final String                        protocolId;
  private final int                           totalCandidateCount;
  private final BulletinBoard                 bulletinBoard;
  private final GeneralAlgorithms             generalAlgorithms;
  private final KeyEstablishmentAlgorithms    keyEstablishmentAlgorithms;
  private final TallyingAuthoritiesAlgorithm  tallyingAuthoritiesAlgorithm;
  private final DecryptionAuthorityAlgorithms decryptionAuthorityAlgorithms;
  private final ElectionSetWithPublicKey      electionSet;
  private final List<Voter>                   voters;
  private       EncryptionPrivateKey          electionOfficerPrivateKey;
  private       EncryptionPublicKey           electionOfficerPublicKey;

  ElectionAdministrationSimulator(String protocolId, int totalCandidateCount,
                                  BulletinBoard bulletinBoard, GeneralAlgorithms generalAlgorithms,
                                  KeyEstablishmentAlgorithms keyEstablishmentAlgorithms,
                                  TallyingAuthoritiesAlgorithm tallyingAuthoritiesAlgorithm,
                                  DecryptionAuthorityAlgorithms decryptionAuthorityAlgorithms,
                                  ElectionSetWithPublicKey electionSet, List<Voter> voters) {
    this.protocolId = protocolId;
    this.totalCandidateCount = totalCandidateCount;
    this.bulletinBoard = bulletinBoard;
    this.generalAlgorithms = generalAlgorithms;
    this.keyEstablishmentAlgorithms = keyEstablishmentAlgorithms;
    this.tallyingAuthoritiesAlgorithm = tallyingAuthoritiesAlgorithm;
    this.decryptionAuthorityAlgorithms = decryptionAuthorityAlgorithms;
    this.electionSet = electionSet;
    this.voters = voters;
  }

  void generateElectionOfficerKey() {
    PublicParameters publicParameters = bulletinBoard.getPublicParameters(protocolId);
    EncryptionKeyPair keyPair = keyEstablishmentAlgorithms.generateKeyPair(publicParameters.getEncryptionGroup());
    electionOfficerPrivateKey = keyPair.getPrivateKey();
    electionOfficerPublicKey = keyPair.getPublicKey();
  }

  EncryptionPublicKey getElectionOfficerPublicKey() {
    return electionOfficerPublicKey;
  }

  TallyDecryptionAndProof getTallyDecryptionAndProof() throws InvalidDecryptionProofException {
    Preconditions.checkState(electionOfficerPrivateKey != null);
    TallyData tallyData = bulletinBoard.getTallyData(protocolId);
    List<DecryptionProof> decryptionProofs = tallyData.getDecryptionProofs();
    List<BigInteger> publicKeyShares = tallyData.getPublicKeyShares();
    List<Encryption> finalShuffle = tallyData.getFinalShuffle();
    Stopwatch decryptionProofCheckWatch = Stopwatch.createStarted();
    List<Decryptions> partialDecryptions = new ArrayList<>(tallyData.getPartialDecryptions());

    boolean areAllProofsValid = tallyingAuthoritiesAlgorithm.checkDecryptionProofs(decryptionProofs, publicKeyShares,
                                                                                   finalShuffle, partialDecryptions);
    if (!areAllProofsValid) {
      throw new InvalidDecryptionProofException("An invalid decryption proof was found");
    }
    decryptionProofCheckWatch.stop();
    if (perfLog.isInfoEnabled()) {
      perfLog.info("Administration : checked decryption proofs in {}ms",
                   decryptionProofCheckWatch.elapsed(TimeUnit.MILLISECONDS));
    }

    BigInteger privateKey = electionOfficerPrivateKey.getPrivateKey();
    Decryptions electionOfficerDecryptions =
        decryptionAuthorityAlgorithms.getPartialDecryptions(finalShuffle, privateKey);

    BigInteger publicKey = electionOfficerPublicKey.getPublicKey();
    DecryptionProof electionOfficerDecryptionProof =
        decryptionAuthorityAlgorithms
            .genDecryptionProof(privateKey, publicKey, finalShuffle, electionOfficerDecryptions);

    partialDecryptions.add(electionOfficerDecryptions);

    int candidatesCount = electionSet.getCandidates().size();
    List<CountingCircle> countingCircles =
        voters.parallelStream().map(Voter::getCountingCircle).distinct().collect(Collectors.toList());
    List<BigInteger> primes;
    try {
      primes = generalAlgorithms.getPrimes(candidatesCount + countingCircles.size());
    } catch (NotEnoughPrimesInGroupException e) {
      throw new EncryptionColouringRuntimeException("Unable to colour the encryption", e);
    }

    Map<BigInteger, CountingCircle> countingCircleByColour =
        countingCircles.stream().collect(
            Collectors.toMap(cc -> primes.get(candidatesCount + cc.getId()),
                             Function.identity()));

    Decryptions decryptions = tallyingAuthoritiesAlgorithm.getDecryptions(finalShuffle, partialDecryptions);
    Map<CountingCircle, Decryptions> decryptionsByCountingCircle =
        decryptions.getDecryptionsList().parallelStream().collect(
            Collectors.groupingBy(decryption ->
                                      countingCircleByColour.entrySet().parallelStream()
                                                            .filter(
                                                                e -> decryption.mod(e.getKey()).equals(BigInteger.ZERO))
                                                            .findFirst()
                                                            .map(Map.Entry::getValue)
                                                            .orElseThrow(() -> new EncryptionColouringRuntimeException(
                                                                COLOUR_EXCEPTION)),
                                  MoreCollectors.toDecryptions()));

    Map<CountingCircle, List<List<Boolean>>> votes =
        decryptionsByCountingCircle.entrySet().parallelStream().collect(
            Collectors.toMap(Map.Entry::getKey,
                             e -> tallyingAuthoritiesAlgorithm.getVotes(e.getValue(), totalCandidateCount)));
    // Additional verifications on the votes validity may be performed here.
    Map<CountingCircle, List<Long>> tally =
        votes.entrySet().parallelStream()
             .collect(Collectors.toMap(
                 Map.Entry::getKey,
                 e -> IntStream.range(0, totalCandidateCount)
                               .mapToLong(i -> e.getValue().parallelStream().filter(vote -> vote.get(i)).count())
                               .boxed().collect(Collectors.toList())));
    return new TallyDecryptionAndProof(new Tally(tally), electionOfficerDecryptions, electionOfficerDecryptionProof);
  }

}

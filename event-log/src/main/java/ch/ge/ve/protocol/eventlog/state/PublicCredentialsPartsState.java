/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.model.Point;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@code PublicKeyPartsState} is responsible to maintain the state of the public credentials parts for components
 * across their clustered deployment.
 * <p>
 * Public credentials are keyed by their source authority index.
 */
public class PublicCredentialsPartsState extends AbstractDoubleKeyedPropertyState<Point> {

  public PublicCredentialsPartsState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter
      converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  @Override
  protected EventType getEventType() {
    return EventType.PUBLIC_CREDENTIALS_PART_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key, int secondaryKey) {
    return String.format(
        "Once the public credentials have been set for authority j=%d and voter i=%d, they can no longer be changed",
        key, secondaryKey);
  }

  @Override
  protected TypeReference<Point> getPropertyType() {
    return new TypeReference<Point>() {
    };
  }

  @Transactional
  public List<Integer> getVoterIds(String protocolId) {
    return super.getSecondaryKeys(protocolId);
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm

import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE
import static ch.ge.ve.protocol.core.support.BigIntegers.TWO

import ch.ge.ve.protocol.core.exception.ChannelSecurityException
import ch.ge.ve.protocol.core.model.AlgorithmsSpec
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey
import ch.ge.ve.protocol.core.support.Hash
import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.EncryptedMessage
import ch.ge.ve.protocol.model.EncryptedMessageWithSignature
import ch.ge.ve.protocol.model.IdentificationGroup
import ch.ge.ve.protocol.model.IdentificationPublicKey
import ch.ge.ve.protocol.model.Signature
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import java.security.Security
import org.bouncycastle.jce.provider.BouncyCastleProvider
import spock.lang.Specification

/**
 * Tests on the algorithms used for establishing security in channels
 */
class ChannelSecurityAlgorithmsTest extends Specification {
  public static final String MESSAGE = 'some message'
  RandomGenerator randomGenerator = Mock()
  IdentificationGroup identificationGroup = new IdentificationGroup(ELEVEN, FIVE, THREE)
  AlgorithmsSpec algorithmsSpec = new AlgorithmsSpec(
          'AES',
          'BC',
          'AES/CTR/NoPadding',
          'BC',
          'BLAKE2B-256',
          null,
          null
  )

  ChannelSecurityAlgorithms channelSecurity

  void setup() {
    Security.addProvider(new BouncyCastleProvider())
    def hash = new Hash('BLAKE2B-256', 'BC', 28)

    channelSecurity = new ChannelSecurityAlgorithms(
            algorithmsSpec,
            hash,
            identificationGroup,
            randomGenerator
    )
  }

  def "genSignature should throw an error if private key is not in Z_q_hat"() {
    given:
    def sk = BigInteger.valueOf(17)

    when:
    channelSecurity.genSignature(sk, MESSAGE)

    then:
    def ex = thrown(IllegalArgumentException)
    ex.message == 'sk must be in Z_q_hat'
  }

  def "genSignature should generate a signature for the given message"() {
    given:
    def sk = THREE

    when:
    Signature tau = channelSecurity.genSignature(sk, MESSAGE)

    then:
    1 * randomGenerator.randomInZq(FIVE) >> THREE

    tau.c == THREE
    tau.s == FOUR
  }

  def "verifySignature should throw an error if public key is not in G_q_hat"() {
    given:
    def pk = BigInteger.valueOf(17)
    def signature = new Signature(THREE, FOUR)

    when:
    channelSecurity.verifySignature(pk, signature, MESSAGE)

    then:
    def ex = thrown(IllegalArgumentException)
    ex.message == 'pk must be in G_q_hat'
  }

  def "verifySignature should throw an error if signature's c is not in Z_q_hat"() {
    given:
    def pk =FIVE
    def signature = new Signature(BigInteger.valueOf(17), FOUR)

    when:
    channelSecurity.verifySignature(pk, signature, MESSAGE)

    then:
    def ex = thrown(IllegalArgumentException)
    ex.message == 'c must be in Z_q_hat'
  }

  def "verifySignature should throw an error if signature's s is not in Z_q_hat"() {
    given:
    def pk = FIVE
    def signature = new Signature(THREE, BigInteger.valueOf(17))

    when:
    channelSecurity.verifySignature(pk, signature, MESSAGE)

    then:
    def ex = thrown(IllegalArgumentException)
    ex.message == 's must be in Z_q_hat'
  }

  def "verifySignature should return true is the signature corresponds to the message"() {
    given:
    def pk = FIVE
    def signature = new Signature(THREE, FOUR)

    expect:
    channelSecurity.verifySignature(pk, signature, MESSAGE)
  }

  def "chaining generate and verify signature should be successful"() {
    given:
    def sk = THREE
    def pk = FIVE
    randomGenerator.randomInZq(FIVE) >> THREE
    def message = MESSAGE

    when:
    boolean check = channelSecurity.verifySignature(pk, channelSecurity.genSignature(sk, message), message)

    then:
    check
  }

  def "chaining generate and verify signature on complex key should be successful"() {
    given:

    ObjectMapper mapper = new ObjectMapper()
    SimpleModule module = new SimpleModule()
    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer())
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())
    mapper.registerModule(module)

    IdentificationPublicKey pk = mapper.readValue(
            ChannelSecurityAlgorithmsTest.class.getResourceAsStream("/level2-public.pk"),
            IdentificationPublicKey.class
    )

    IdentificationPrivateKey sk = mapper.readValue(
            ChannelSecurityAlgorithmsTest.class.getResourceAsStream("/level2-private.sk"),
            IdentificationPrivateKey.class
    )

    def hash = new Hash("BLAKE2B-256", "BC", 28)
    def complexChannelSecurity = new ChannelSecurityAlgorithms(
            algorithmsSpec,
            hash,
            (IdentificationGroup) pk.getCyclicGroup(),
            randomGenerator
    )

    randomGenerator.randomInZq(_) >> THREE
    def message = MESSAGE

    when:
    boolean check = complexChannelSecurity.verifySignature(pk.getPublicKey(), complexChannelSecurity.genSignature(sk.privateKey, message), message)

    then:
    check
  }

  def "genCiphertext should throw an error if public key is not in G_q_hat"() {
    given:
    def pk = BigInteger.valueOf(17)

    when:
    channelSecurity.genCiphertext(pk, MESSAGE)

    then:
    def ex = thrown(IllegalArgumentException)
    ex.message == 'pk must be in G_q_hat'
  }

  def "genCiphertext should generate an encrypted message for the given object"() {
    given:
    def pk = FIVE

    when:
    EncryptedMessage encryptedMessage = channelSecurity.genCiphertext(pk, MESSAGE)

    then:
    1 * randomGenerator.randomInZq(FIVE) >> THREE

    encryptedMessage.c1 == FIVE
  }

  def "getPlaintext should throw an error if private key is not in Z_q_hat"() {
    given:
    def sk = BigInteger.valueOf(17)
    def encryptedMessage = new EncryptedMessage(FIVE, [] as byte[])

    when:
    channelSecurity.getPlaintext(sk, encryptedMessage)

    then:
    def ex = thrown(IllegalArgumentException)
    ex.message == 'sk must be in Z_q_hat'
  }

  def "getPlaintext should throw an error if c1 is not in G_q_hat"() {
    given:
    def sk = THREE
    def encryptedMessage = new EncryptedMessage(BigInteger.valueOf(17), [] as byte[])

    when:
    channelSecurity.getPlaintext(sk, encryptedMessage)

    then:
    def ex = thrown(IllegalArgumentException)
    ex.message == 'c1 must be in G_q_hat'
  }

  def "chaining encrypt and decrypt should return the original object"() {
    given:
    def sk = THREE
    def pk = FIVE
    randomGenerator.randomInZq(FIVE) >> THREE

    when:
    String decrypted = channelSecurity.getPlaintext(sk, channelSecurity.genCiphertext(pk, MESSAGE))

    then:
    decrypted == MESSAGE
  }

  def "signAndEncrypt should generate a message's signature and its cipher"() {
    given:
    def sk = THREE
    def pk = FIVE
    def message = MESSAGE

    when:
    def cipherWithSignature = channelSecurity.signAndEncrypt(sk, pk, message)

    then:
    2 * randomGenerator.randomInZq(FIVE) >>> [THREE]
    cipherWithSignature.signature.c == THREE
    cipherWithSignature.signature.s == FOUR
    cipherWithSignature.encryptedMessage.c1 == FIVE
  }

  def "decryptAndVerify should throw an error if the signature could not be verified"() {
    given:
    def sk = THREE
    def pk = FIVE
    def message = MESSAGE
    randomGenerator.randomInZq(FIVE) >>> [THREE]

    when:
    def encryptedMessageWithSignature = channelSecurity.signAndEncrypt(sk, pk, message)
    channelSecurity.decryptAndVerify(sk, pk,
            new EncryptedMessageWithSignature(encryptedMessageWithSignature.encryptedMessage, new Signature(TWO, FOUR)))

    then:
    def ex = thrown(ChannelSecurityException)
    ex.message == 'signature could not be verified'
  }

  def "chaining signAndEncrypt and decryptAndVerify should return the original object"() {
    given:
    def sk = THREE
    def pk = FIVE
    def message = MESSAGE
    randomGenerator.randomInZq(FIVE) >>> [THREE]

    when:
    String decrypted = channelSecurity.decryptAndVerify(sk, pk, channelSecurity.signAndEncrypt(sk, pk, message))

    then:
    decrypted == message
  }

  final class BigIntegerAsBase64Serializer extends StdSerializer<BigInteger> {
    BigIntegerAsBase64Serializer() {
      super(BigInteger.class)
    }

    @Override
    void serialize(BigInteger bigInteger, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
      jsonGenerator.writeString(Base64.getEncoder().encodeToString(bigInteger.toByteArray()))
    }
  }

  final class BigIntegerAsBase64Deserializer extends StdDeserializer<BigInteger> {
    BigIntegerAsBase64Deserializer() {
      super(BigInteger.class)
    }

    @Override
    BigInteger deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
      return new BigInteger(Base64.getDecoder().decode(jsonParser.getValueAsString()))
    }
  }

}

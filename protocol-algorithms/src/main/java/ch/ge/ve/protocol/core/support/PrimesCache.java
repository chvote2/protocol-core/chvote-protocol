/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.support;

import ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.model.EncryptionGroup;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class PrimesCache {
  /**
   * Creates a new {@code PrimesCache}, computing the requested number of primes.
   *
   * @param n               the number of primes to compute.
   * @param encryptionGroup the encryption group.
   *
   * @return the created {@link PrimesCache}.
   *
   * @throws NullPointerException            if {@code encryptionGroup} is {@code null}.
   * @throws NotEnoughPrimesInGroupException if the encryption group is too small to yield the requested number of
   *                                         primes.
   */
  public static PrimesCache populate(int n, EncryptionGroup encryptionGroup) throws NotEnoughPrimesInGroupException {
    Preconditions.checkArgument(n >= 0, "n must be greater or equal to 0");
    BigInteger x = BigInteger.ONE;
    List<BigInteger> primes = new ArrayList<>();
    int i = 0;
    while (i < n) {
      do {
        // Performance improvement over +1 / +2 defined in algorithm
        x = x.nextProbablePrime();
        if (x.compareTo(encryptionGroup.getP()) >= 0) {
          throw new NotEnoughPrimesInGroupException(
              String.format("Only found %d primes (%s) in group %s",
                            i,
                            primes.stream().limit(4).map(BigInteger::toString).collect(Collectors.joining(",")),
                            encryptionGroup));
        }
      } while (!x.isProbablePrime(100) || !isMember(x, encryptionGroup));
      primes.add(x);
      i++;
    }
    return new PrimesCache(primes, encryptionGroup);
  }

  private static boolean isMember(BigInteger x, EncryptionGroup encryptionGroup) {
    return x.compareTo(BigInteger.ONE) >= 0 && x.compareTo(encryptionGroup.getP()) < 0
           && BigIntegerArithmetic.jacobiSymbol(x, encryptionGroup.getP()) == 1;
  }

  private final List<BigInteger> primes;
  private final EncryptionGroup  encryptionGroup;

  /**
   * Creates a new {@code PrimesCache}.
   *
   * @param primes          the primes to cache.
   * @param encryptionGroup the encryption group used to generate the given primes.
   *
   * @throws NullPointerException     if on of the arguments is {@code null}.
   * @throws IllegalArgumentException if the given primes are not members of the given encryption group.
   */
  public PrimesCache(List<BigInteger> primes, EncryptionGroup encryptionGroup) {
    Preconditions.checkNotNull(primes);
    Preconditions.checkNotNull(encryptionGroup);
    this.primes = primes.stream().sorted().collect(Collectors.toList());
    this.encryptionGroup = encryptionGroup;
    Preconditions.checkArgument(this.primes.stream().allMatch(x -> isMember(x, encryptionGroup)));
  }

  /**
   * Returns the cached prime numbers.
   *
   * @return the cached prime numbers.
   */
  public List<BigInteger> getPrimes() {
    return primes;
  }

  /**
   * Returns the encryption group used to generate the primes.
   *
   * @return the encryption group used to generate the primes.
   */
  public EncryptionGroup getEncryptionGroup() {
    return encryptionGroup;
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm;

import static ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic.modExp;

import ch.ge.ve.protocol.core.model.PointsAndZeroImage;
import ch.ge.ve.protocol.core.model.SecretVoterData;
import ch.ge.ve.protocol.core.model.VoterData;
import ch.ge.ve.protocol.core.support.ByteArrayUtils;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.ElectionSet;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrintingAuthority;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Voter;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Algorithms relevant to the election preparation
 */
public class ElectionPreparationAlgorithms {
  private final Hash                 hash;
  private final RandomGenerator      randomGenerator;
  private final IdentificationGroup  identificationGroup;
  private final PolynomialAlgorithms polynomialAlgorithms;
  private final int                  s;
  private final PublicParameters     publicParameters;

  public ElectionPreparationAlgorithms(PublicParameters publicParameters, RandomGenerator randomGenerator, Hash hash,
                                       PolynomialAlgorithms polynomialAlgorithms) {
    this.hash = hash;
    this.randomGenerator = randomGenerator;
    this.publicParameters = publicParameters;
    identificationGroup = publicParameters.getIdentificationGroup();
    this.polynomialAlgorithms = polynomialAlgorithms;
    s = publicParameters.getS();
  }

  public PolynomialAlgorithms getPolynomialAlgorithms() {
    return polynomialAlgorithms;
  }

  /**
   * Algorithm 7.6: GenElectorateData <p>Generates the code sheet data for the whole electorate.</p>
   * <p>
   * This has been adapted from the specification to handle voters by batches.
   *
   * @param electionSet contains all three of <b>bold_n</b>, <b>bold_k</b> and <b>bold_upper_e</b>
   *
   * @return the generated electorate data, including private and public voter data, as well as the matrix bold_upper_k
   * derived from bold_k and bold_upper_e
   */
  public List<VoterData> genElectorateData(List<Voter> voters, ElectionSet<? extends PrintingAuthority> electionSet) {
    Map<Integer, VoterData> voterDataMap =
        voters.stream().parallel()
              .collect(Collectors.toMap(
                  Voter::getVoterId,
                  voter -> {
                    List<Election> elections = electionSet.getElections();
                    List<Integer> bold_k_i = elections
                        .stream()
                        .map(e -> electionSet.isEligible(voter, e) ? e.getNumberOfSelections() : 0)
                        .collect(Collectors.toList());
                    int k_i = electionSet.getElections().stream()
                                         .filter(e -> electionSet.isEligible(voter, e))
                                         .mapToInt(Election::getNumberOfSelections)
                                         .sum();

                    List<Integer> bold_n = electionSet.getElections().stream().map(Election::getNumberOfCandidates)
                                                      .collect(Collectors.toList());
                    List<Boolean> bold_e = electionSet.getElections().stream()
                                                      .map(election -> electionSet.isEligible(voter, election))
                                                      .collect(Collectors.toList());
                    PointsAndZeroImage pointsAndZeroImage =
                        polynomialAlgorithms.genPoints(bold_n, bold_e, k_i);
                    SecretVoterData d_i =
                        genSecretVoterData(voter, pointsAndZeroImage.getPoints());
                    return new VoterData(voter.getVoterId(), d_i,
                                         getPublicVoterData(d_i.getX(), d_i.getY(), pointsAndZeroImage.getY_prime()),
                                         pointsAndZeroImage.getPoints(),
                                         bold_k_i);
                  }
              ));

    return voters.stream()
                 .map(Voter::getVoterId)
                 .sorted()
                 .map(voterDataMap::get).collect(Collectors.toList());
  }

  /**
   * Algorithm 7.10: GenSecretVoterData
   *
   * @param voter  the voter for whom the data is being generated
   * @param bold_p a list of points, p_i \in Z^2_{p'}
   *
   * @return the secret data for a single voter
   */
  public SecretVoterData genSecretVoterData(Voter voter, List<Point> bold_p) {
    BigInteger bigIntS = BigInteger.valueOf(publicParameters.getS());
    BigInteger q_hat_prime_x = publicParameters.getQ_hat_x().divide(bigIntS);
    BigInteger q_hat_prime_y = publicParameters.getQ_hat_y().divide(bigIntS);

    BigInteger x = randomGenerator.randomInZq(q_hat_prime_x);
    BigInteger y = randomGenerator.randomInZq(q_hat_prime_y);
    Object[] nonNullPointsArray = bold_p.stream().filter(Objects::nonNull).toArray();
    byte[] upper_f = ByteArrayUtils.truncate(hash.recHash_L(nonNullPointsArray), publicParameters.getUpper_l_f());
    byte[][] rc = new byte[bold_p.size()][];

    for (int i = 0; i < bold_p.size(); i++) {
      Point point = bold_p.get(i);
      if (point != null) {
        rc[i] = ByteArrayUtils.truncate(hash.recHash_L(point), publicParameters.getUpper_l_r());
      } else {
        rc[i] = new byte[0];
      }
    }

    return new SecretVoterData(voter, x, y, upper_f, rc);
  }

  /**
   * Algorithm 7.11: GetPublicVoterData
   *
   * @param x       secret voting credential x &isin; &integers;_q_hat
   * @param y       secret confirmation credential y &isin; &integers;_q_hat
   * @param y_prime secret vote validity credential y' &isin; &integers;_p'
   *
   * @return the public data for a single voter, sent to the bulletin board
   */
  public Point getPublicVoterData(BigInteger x, BigInteger y, BigInteger y_prime) {
    BigInteger p_hat = identificationGroup.getP_hat();
    BigInteger q_hat = identificationGroup.getQ_hat();
    BigInteger g_hat = identificationGroup.getG_hat();

    BigInteger x_hat = modExp(g_hat, x, p_hat);
    BigInteger y_exponent = y.add(y_prime).mod(q_hat);
    BigInteger y_hat = modExp(g_hat, y_exponent, p_hat);

    return new Point(x_hat, y_hat);
  }

  /**
   * Algorithm 7.12: GetPublicCredentials
   *
   * @param bold_upper_d_hat the public voter data generated by each of the authorities
   *
   * @return the combined public voter credentials (one point per voter)
   */
  public List<Point> getPublicCredentials(List<List<Point>> bold_upper_d_hat) {
    Preconditions.checkArgument(bold_upper_d_hat.size() == s,
                                String.format("|bold_upper_d_hat| [%d] != s [%d]", bold_upper_d_hat.size(), s));
    return IntStream.range(0, bold_upper_d_hat.get(0).size()).mapToObj(i -> {
      BigInteger x_hat_i = BigInteger.ONE;
      BigInteger y_hat_i = BigInteger.ONE;
      for (List<Point> d_hat_j : bold_upper_d_hat) {
        Point voterData_ij = d_hat_j.get(i);
        x_hat_i = x_hat_i.multiply(voterData_ij.x).mod(identificationGroup.getP_hat());
        y_hat_i = y_hat_i.multiply(voterData_ij.y).mod(identificationGroup.getP_hat());
      }
      return new Point(x_hat_i, y_hat_i);
    }).collect(Collectors.toList());
  }
}

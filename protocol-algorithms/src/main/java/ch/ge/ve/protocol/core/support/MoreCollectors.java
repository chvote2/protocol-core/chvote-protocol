/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.support;

import ch.ge.ve.protocol.model.Decryptions;
import java.math.BigInteger;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Additional collectors to be used for collecting specific types.
 */
public class MoreCollectors {
  private MoreCollectors() {
  }

  /**
   * @return a collector transforming a stream of {@code List<BigInteger>} to a new {@link Decryptions} instance.
   */
  public static Collector<BigInteger, ?, Decryptions> toDecryptions() {
    return Collectors.collectingAndThen(Collectors.toList(), Decryptions::new);
  }
}

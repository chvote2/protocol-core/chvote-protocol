/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.GeneratorsPublicationEvent;
import ch.ge.ve.event.ShufflePublicationEvent;
import ch.ge.ve.event.payload.ShufflePayload;
import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.protocol.core.model.ShuffleAndProof;
import ch.ge.ve.service.AbstractAcknowledgeableEventsListener;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Bulletin board shuffling events listener.
 */
@Component
public class BulletinBoardShufflingListener extends AbstractAcknowledgeableEventsListener {
  private static final Logger logger = LoggerFactory.getLogger(BulletinBoardShufflingListener.class);

  private final BulletinBoard bulletinBoard;

  @Autowired
  public BulletinBoardShufflingListener(BulletinBoard bulletinBoard, EventBus eventBus) {
    super(eventBus, Endpoints.BULLETIN_BOARD);
    this.bulletinBoard = bulletinBoard;
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.GENERATORS_PUBLICATIONS + "-bulletinBoard", durable = "true",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX)),
          exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true", value = Channels.GENERATORS_PUBLICATIONS)
      )
  )
  public void processGeneratorsPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                GeneratorsPublicationEvent event) {
    logger.info("Received generators publication event");
    bulletinBoard.storeGenerators(protocolId, event.getPayload().getGenerators());
    acknowledge(protocolId, event);
  }

  @RabbitListener(
      bindings = @QueueBinding(
          value = @Queue(value = Channels.SHUFFLE_PUBLICATIONS + "-bulletinBoard", durable = "true",
                         arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX)),
          exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true", value = Channels.SHUFFLE_PUBLICATIONS)
      )
  )
  public void processShufflePublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                             ShufflePublicationEvent event) {
    ShufflePayload payload = event.getPayload();
    int ccIndex = payload.getControlComponentIndex();
    logger.info("Received shuffle from control component #{}", ccIndex);
    ShuffleAndProof shuffleAndProof = payload.getShuffleAndProof();
    bulletinBoard.storeShuffleAndProof(protocolId, ccIndex, shuffleAndProof.getShuffle(), shuffleAndProof.getProof());
    acknowledge(protocolId, event);
  }
}

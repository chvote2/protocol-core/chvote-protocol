/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.config;

import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AlgorithmsSpecConfiguration {

  @Bean
  public AlgorithmsSpec algorithmsSpec(
      @Value("${ch.ge.ve.keyspec.algorithm}") String keySpecAlgorithm,
      @Value("${ch.ge.ve.cipher.provider}") String cipherProvider,
      @Value("${ch.ge.ve.cipher.algorithm}") String cipherAlgorithm,
      @Value("${ch.ge.ve.digest.provider}") String digestProvider,
      @Value("${ch.ge.ve.digest.algorithm}") String digestAlgorithm,
      @Value("${ch.ge.ve.rng.provider}") String randomGeneratorProvider,
      @Value("${ch.ge.ve.rng.algorithm}") String randomGeneratorAlgorithm) {
    return new AlgorithmsSpec(
        keySpecAlgorithm,
        cipherProvider,
        cipherAlgorithm,
        digestProvider,
        digestAlgorithm,
        randomGeneratorProvider,
        randomGeneratorAlgorithm
    );
  }
}

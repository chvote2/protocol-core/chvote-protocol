/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service

import ch.ge.ve.event.AbstractAcknowledgeableEvent
import ch.ge.ve.event.AbstractAcknowledgeableMulticastEvent
import ch.ge.ve.event.AcknowledgeableEvent
import ch.ge.ve.event.SimpleAcknowledgementEvent
import ch.ge.ve.event.SimpleMulticastAcknowledgementEvent
import ch.ge.ve.event.UnacknowledgedEvent
import ch.ge.ve.event.payload.EmptyPayload
import com.google.common.collect.ImmutableList
import spock.lang.Shared
import spock.lang.Specification

class AcknowledgementServiceInMemoryImplTest extends Specification {

  private static final String PROTOCOL_ID = "0"

  @Shared
  AcknowledgementService service

  @Shared
  UnacknowledgedEvent unicastUnacknowledgedEvent

  @Shared
  UnacknowledgedEvent multicastUnacknowledgedEvent

  void setup() {
    service = new AcknowledgementServiceInMemoryImpl()
    unicastUnacknowledgedEvent = unacknowledgedEvent("1", "emitter")
    multicastUnacknowledgedEvent = unacknowledgedEvent("2", "emitter", "part1", "part2")
    service.registerEvent(unicastUnacknowledgedEvent)
    service.registerEvent(multicastUnacknowledgedEvent)
  }

  def "it should return all unacknowledged events"() {
    given: "an acknowledgement service with 2 registered events"

    when: "the unacknowledged events are requested"
    def unacknowledgedEvents = service.getUnacknowledgedEvents()

    then: "the service should return the unacknowledged events"
    unacknowledgedEvents.size() == 2
    unacknowledgedEvents.contains(unicastUnacknowledgedEvent)
    unacknowledgedEvents.contains(multicastUnacknowledgedEvent)
  }

  def "a simple acknowledgeable event should be acknowledged by a single acknowledgement event"() {
    given: "an acknowledgement service with a registered unicast event"
    def event = unicastUnacknowledgedEvent.event

    expect:
    service.isUnacknowledged(event)

    when: "a simple acknowledgeable event is acknowledged"
    service.registerAcknowledgement(new SimpleAcknowledgementEvent(event.eventId))

    then: "the event should be acknowledged"
    !service.isUnacknowledged(event)
  }

  def "a multicast acknowledgeable should be acknowledged by a multiple acknowledgement events"() {
    given: "an acknowledgement service with a registered multicast event with 2 parts"
    def event = multicastUnacknowledgedEvent.event

    expect:
    service.isUnacknowledged(event)

    when: "a multicast acknowledgement event is registered"
    service.registerAcknowledgement(new SimpleMulticastAcknowledgementEvent(event.eventId, "part1"))

    then: "the event is not acknowledged"
    service.isUnacknowledged(event)

    when: "a second multicast acknowledgement event registered"
    service.registerAcknowledgement(new SimpleMulticastAcknowledgementEvent(event.eventId, "part2"))

    then: "the event is not acknowledged"
    !service.isUnacknowledged(event)
  }

  def "unregistering an event should remove it from unacknowledged events"() {
    given: "an acknowledgement service with 2 registered events"

    expect:
    service.getUnacknowledgedEvents().size() == 2

    when: "an event is unregistered"
    service.forget(multicastUnacknowledgedEvent.event)

    then: "the event should be removed from unacknowledged events"
    service.getUnacknowledgedEvents().size() == 1
    !service.getUnacknowledgedEvents().contains(multicastUnacknowledgedEvent)
  }

  def "unregistering events for a protocol id should remove all unacknowledged events for that protocol"() {
    given: "an acknowledgement service with 2 registered events for a same protocol id"

    expect:
    service.getUnacknowledgedEvents().size() == 2

    when: "all events for a protocol is unregistered"
    service.forget(PROTOCOL_ID)

    then: "all events for that should be removed from unacknowledged events"
    service.getUnacknowledgedEvents().empty
  }

  UnacknowledgedEvent unacknowledgedEvent(String eventId, String emitter, String... partIndexes) {
    AcknowledgeableEvent acknowledgeableEvent
    if (partIndexes.length == 0) {
      acknowledgeableEvent = new SimpleAcknowledgeableEvent(eventId, emitter)
    } else {
      acknowledgeableEvent = new SimpleMulticastAcknowledgeableEvent(eventId, emitter, partIndexes)
    }
    return new UnacknowledgedEvent(PROTOCOL_ID, "test-emitter", "test-channel", "", acknowledgeableEvent)
  }

  private static final class SimpleAcknowledgeableEvent extends AbstractAcknowledgeableEvent {
    SimpleAcknowledgeableEvent(String eventId, String emitter) {
      super(eventId, emitter, new EmptyPayload())
    }
  }

  private static final class SimpleMulticastAcknowledgeableEvent extends AbstractAcknowledgeableMulticastEvent {
    SimpleMulticastAcknowledgeableEvent(String eventId, String emitter, String... acknowledgementPartIndexes) {
      super(eventId, emitter, ImmutableList.copyOf(acknowledgementPartIndexes), new EmptyPayload())
    }
  }
}

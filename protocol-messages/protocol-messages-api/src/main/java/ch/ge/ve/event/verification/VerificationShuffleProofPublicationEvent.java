/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.verification;


import ch.ge.ve.event.Event;
import ch.ge.ve.protocol.model.ShuffleProof;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Event used during verification for publishing the shuffle proof for a given control component index.
 */
public final class VerificationShuffleProofPublicationEvent implements Event {

  private final int          ccIndex;
  private final ShuffleProof shuffleProof;

  @JsonCreator
  public VerificationShuffleProofPublicationEvent(@JsonProperty("ccIndex") int ccIndex,
                                                  @JsonProperty("shuffleProof") ShuffleProof shuffleProof) {
    this.ccIndex = ccIndex;
    this.shuffleProof = shuffleProof;
  }

  public int getCcIndex() {
    return ccIndex;
  }

  public ShuffleProof getShuffleProof() {
    return shuffleProof;
  }
}

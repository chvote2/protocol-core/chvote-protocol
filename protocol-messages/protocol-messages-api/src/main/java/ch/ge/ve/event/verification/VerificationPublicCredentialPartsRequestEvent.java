/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.verification;

import ch.ge.ve.event.Event;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 * Event used during verification for requesting public credential parts,
 * by control component index and in slices of voter ids.
 */
public final class VerificationPublicCredentialPartsRequestEvent implements Event {

  private final int           ccIndex;
  private final List<Integer> voterIds;

  @JsonCreator
  public VerificationPublicCredentialPartsRequestEvent(@JsonProperty("ccIndex") int ccIndex,
                                                       @JsonProperty("voterIds") List<Integer> voterIds) {
    this.ccIndex = ccIndex;
    this.voterIds = ImmutableList.copyOf(voterIds);
  }

  public int getCcIndex() {
    return ccIndex;
  }

  public List<Integer> getVoterIds() {
    return voterIds;
  }
}

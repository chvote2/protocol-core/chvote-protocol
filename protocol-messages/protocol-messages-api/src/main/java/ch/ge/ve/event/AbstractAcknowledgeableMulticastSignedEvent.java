/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.Payload;
import ch.ge.ve.protocol.model.Signature;
import com.google.common.base.Preconditions;
import java.util.List;
import java.util.UUID;

/**
 * Abstract skeleton implementation of {@link AcknowledgeableMulticastEvent} and {@link SignedEvent}.
 */
public abstract class AbstractAcknowledgeableMulticastSignedEvent<P extends Payload>
    extends AbstractAcknowledgeableMulticastEvent<P> implements SignedEvent<P> {

  private final Signature signature;
  private final String    verificationKeyHash;

  protected AbstractAcknowledgeableMulticastSignedEvent(String emitter, List<String> acknowledgementPartIndexes,
                                                        P payload, Signature signature, String verificationKeyHash) {
    this(UUID.randomUUID().toString(), emitter, acknowledgementPartIndexes, payload, signature, verificationKeyHash);
  }

  protected AbstractAcknowledgeableMulticastSignedEvent(String eventId, String emitter,
                                                        List<String> acknowledgementPartIndexes,
                                                        P payload, Signature signature, String verificationKeyHash) {
    super(eventId, emitter, acknowledgementPartIndexes, payload);
    Preconditions.checkNotNull(signature, "signature must not be null");
    Preconditions.checkNotNull(verificationKeyHash, "verificationKeyHash must not be null");
    this.signature = signature;
    this.verificationKeyHash = verificationKeyHash;
  }

  @Override
  public Signature getSignature() {
    return signature;
  }

  @Override
  public String getVerificationKeyHash() {
    return verificationKeyHash;
  }
}

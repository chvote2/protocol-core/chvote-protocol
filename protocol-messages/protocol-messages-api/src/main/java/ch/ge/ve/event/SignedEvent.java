/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.Payload;
import ch.ge.ve.protocol.model.Signature;

/**
 * Event for which a digital signature is expected.
 */
public interface SignedEvent<P extends Payload> extends Event {

  /**
   * Returns the event's payload (part of the event covered by the signature).
   *
   * @return the event's payload.
   */
  P getPayload();

  /**
   * Returns the payload's signature.
   *
   * @return the payload's signature.
   */
  Signature getSignature();

  /**
   * Returns the hash of the signing key's public part.
   *
   * @return the hash of the signing key's public part.
   */
  String getVerificationKeyHash();
}

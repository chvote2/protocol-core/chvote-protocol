/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.VoterIdsPayload;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Event used for requesting the building of some voters public credentials.
 * <p>Once the voters' public credentials parts have been published by all control components,
 * they may be combined into their full public credentials.</p>
 */
public final class BuildPublicCredentialsRequestEvent extends AbstractAcknowledgeableEvent<VoterIdsPayload> {

  public BuildPublicCredentialsRequestEvent(String emitter, VoterIdsPayload payload) {
    super(emitter, payload);
  }

  @JsonCreator
  public BuildPublicCredentialsRequestEvent(@JsonProperty("eventId") String eventId,
                                            @JsonProperty("emitter") String emitter,
                                            @JsonProperty("payload") VoterIdsPayload payload) {
    super(eventId, emitter, payload);
  }
}

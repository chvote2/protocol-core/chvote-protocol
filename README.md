# CHVote-2.0 protocol core
[![pipeline status](https://gitlab.com/chvote2/protocol-core/chvote-protocol/badges/master/pipeline.svg)](https://gitlab.com/chvote2/protocol-core/chvote-protocol/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-protocol&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-protocol)

The protocol core provides the components of the voting protocol:
- the control components
- the bulletin board
- a protocol client

# Documentation
- [Design documentation](https://gitlab.com/chvote2/protocol-core/chvote-protocol/-/jobs/artifacts/master/raw/protocol-docs/target/generated-docs/pdf/protocol-application-design.pdf?job=build:docs)
- [Threat model](https://gitlab.com/chvote2/protocol-core/chvote-protocol/-/jobs/artifacts/master/raw/protocol-docs/target/generated-docs/pdf/control-components-threat-model.pdf?job=build:docs)

# Development guidelines
- [Coding style for Java development](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/coding-style-Java.md)

# Components

## bulletin-board
The Bulletin Board is a Spring boot application running AMQP listeners. It provides the interactions required
by the voting protocol for the bulletin board, i.e. storing the public data of the protocol and supplying it
to the control components and to the protocol clients when requested.

## control-component
The Control component is a Spring boot application running AMQP listeners. It provides the interactions required
by the voting protocol for the control components:
- setting up the protocol cryptographic material (public parameters, protocol private keys)
- receiving the election and voters data
- generating the voters credentials (identification, verification, confirmation and finalization codes)
- receiving the election officers public keys and setting up the ballots encryption public key
- receiving ballots and their confirmations
- shuffling and partially decrypting the ballot box

## event-log
A shared library dedicated to store the bulletin board and the control component states as a list
of events. It provides an API to append and delete events, an API to query the events, some shared
event definitions, and the persistence layer.

## protocol-algorithms
A shared library implementing the algorithms defined in the
[protocol specification in version 1.4.1](https://eprint.iacr.org/eprint-bin/getfile.pl?entry=2017/325&version=20180502:092201&file=325.pdf).

## protocol-client
A shared library supplying a protocol client implementation.

## protocol-docs
Documentation of the protocol core project.

## protocol-messages
A shared library supplying an API to abstract the interactions with the underlying AMQP message broker, including:
- publishing events,
- managing application level acknowledgements,
- managing the signature of the events (creation and verification),
- managing a dead letter exchange.

## protocol-persistence
A shared library supplying a naming strategy used to set different table names for the same entity shared 
between different components.

## protocol-runner
The protocol runner is an application to run a protocol client against an already deployed protocol components
setup. It covers all the steps of the protocol, from the initialization to the final tally, including
the voting phase. It provides an `injector` profile that allows to offload the client side voting phase
computations to several machines.

This only serves for simulations and test runs, and is never ever meant to be deployed in production environment.

## protocol-support
A shared library supplying services to write data of the protocol to json files.

## simulation
A low level simulation library of the protocol that skips the messaging layer to provide fast feedback
on any algorithm or service layer changes.

## system-tests
System test of the protocol components covering all the steps of the protocol, from the initialization to the 
final tally.

# Building

## Pre-requisites
The following prerequisites must be installed on the development environment:
* JDK 8+
* Latest Docker - https://www.docker.com/get-docker
    > For Windows 7:
    >- https://docs.docker.com/toolbox/toolbox_install_windows.
    >- **RECOMMENDED**: Make sure to check the NDIS5 option when installing Docker Toolbox

Furthermore, the applications need some dependencies to run:
* The Bulletin board and the Control components need a RabbitMQ message broker instance.
* Each Control component and the Bulletin board need a dedicated PostgreSQL database instance to run.

We provide some docker-compose files to run them (see _Running_ chapter).

### Oracle ojdbc driver

To compile the bulletin-board and the protocol-runner components, the Oracle JDBC driver is required. Indeed,
the bulletin board is designed to be deployed for both Oracle and PostgreSQL databases.

You should obtain the driver either:

- by downloading it directly from [Oracle 12.2.0.1 JDBC driver download page](https://www.oracle.com/technetwork/database/features/jdbc/jdbc-ucp-122-3110062.html).
Once you've downloaded ojdbc8.jar, it must be installed to your local maven repository:
```
mvn install:install-file \
-Dfile=<Path where the jar is, example downloads>/ojdbc8.jar \
-DgroupId=com.oracle.jdbc \
-DartifactId=ojdbc8 \
-Dversion=12.2.0.1 \
-Dpackaging=jar \
-DgeneratePom=true
```
- by setting up maven to connect to the Oracle Maven Repository. Please read the 
[Oracle relevant documentation](https://docs.oracle.com/middleware/1213/core/MAVEN/config_maven_repo.htm#MAVEN9010).

A third option is just to remove the ojdbc8 dependencies from the pom.xml files as their are just `runtime` dependencies.

## Build steps
On a Linux host add the following environment variables to your maven runner settings:
```
DOCKER_COMPOSE_LOCATION=<your docker-compose installation directory>
SPRING_PROFILES_ACTIVE=dev
CC_INDEX=0
```

For Windows Docker machine users, add the following environment variables
(adapt with the results from running the command `docker-machine env`):
```
DOCKER_LOCATION=C:\Program Files\Docker Toolbox\docker.exe
DOCKER_TLS_VERIFY=1
DOCKER_MACHINE_NAME=default
COMPOSE_CONVERT_WINDOWS_PATHS=true
DOCKER_HOST=tcp://192.168.99.100:2376
DOCKER_CERT_PATH=C:\Users\<your username>\.docker\machine\machines\default
DOCKER_COMPOSE_LOCATION=C:\Program Files\Docker Toolbox\docker-compose.exe
SPRING_PROFILES_ACTIVE=dev
CC_INDEX=0
SPRING_RABBITMQ_HOST=<your RabbitMQ container host>
```

Run `mvn verify` to run the unit tests and the Spring Boot integration tests.

Run `mvn -P system-tests verify` to build the Docker images and run only the system tests. This will also install
locally the PostgreSQL database image with the needed schema to run the control components and the bulletin board.

# Running
The message broker and the PostgreSQL instances should be started before the applications.

```
docker-compose -f system-tests/docker-compose.yml -p systemtests up
```

>For IntelliJ IDEA users, create a Docker run configuration:
>* new -> docker deployment
>* Server: Docker
>* Deployment: system-tests\docker-compose.yml
>
>*Note: IntelliJ automatically sets the compose project name from the name of the parent directory of the compose file.*

In development phase you should run the applications directly with Spring Boot rather than with Maven and Docker.
The applications must be run with the following Spring profile and environment variables:

|Application|Active profiles|Additional env variables
|-----------|-------|-----|
|BulletinBoardMessagingApplication|dev|SECURITY_LEVEL=_security level_|
|ControlComponentMessagingApplication (0)|dev_0|CC_INDEX=0, SECURITY_LEVEL=_security level_|
|ControlComponentMessagingApplication (1)|dev_1|CC_INDEX=1, SECURITY_LEVEL=_security level_|

>The _SECURITY_LEVEL_ environment variable is optional and will be set to _2_ if not specified

>IntelliJ users:
>You can launch the 3 applications (the bulletin board and the control components) at once using a `Compound` run 
configuration.

When the 3 applications are started, you can run the system test (class ProtocolRunnerApp) with the
parameters as specified in method ProtocolRunner#run. The application must be started with the profiles
``runner,voteclient``.

Finally, here is an example of application parameters to run 100 votes for a SIMPLE_SAMPLE election set:
```
--host=localhost --port=5672 --username=rabbitmq --password=p4ssW0rd 
--nbControlComponents=2 --votersCount=100 --waitTimeout=1200000 
--securityLevel=2 --electionSet=SIMPLE_SAMPLE
```

In all cases, make sure that the message broker and the 3 PostgreSQL databases are 
started, either with the Docker run configuration or from the command line.

# Troubleshooting
If the applications fail to connect to the message broker:
* Check the local IP of the message broker, e.g., 192.168.99.100. On Windows, you typically get it by
  running command `docker-machine ip`.
* Using your browser, check that the message broker responds on its administration port,
  e.g., http://192.168.99.100:15672 (the port is the second one listed in file system-tests\docker-compose.yml).
  
If docker-compose cannot find the chvote/eventlog-database image, run `mvn -P system-tests verify` to
build and install the image locally.

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# Licence
This application is Open Source software released under the [Affero General Public License 3.0](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/LICENCE.txt) 
license.


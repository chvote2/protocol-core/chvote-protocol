package ch.ge.ve.protocol.test;

/**
 * Gives some statistical information about the database state
 */
public interface DatabaseStatsRepository {

  /**
   * Gets the total size in bytes of the database schema.
   *
   * @return the schema size in bytes
   */
  long getSchemaSize(String schema);
}

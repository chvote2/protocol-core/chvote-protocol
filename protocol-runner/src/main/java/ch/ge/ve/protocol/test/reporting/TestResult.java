/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.protocol.test.reporting;

import ch.ge.ve.protocol.client.model.VotePerformanceStats;
import com.google.common.base.Stopwatch;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;

/**
 * The data (parameters, execution times, volumetrics, etc.) of the execution of a test. Intended for creating execution
 * report files.
 */
public class TestResult {
  private static final Logger log = LoggerFactory.getLogger(TestResult.class);

  private static final String PHASE_VOTER_PREPARE_BALLOT           = "Prepare ballot (client side)";
  private static final String PHASE_VOTER_PUBLISH_BALLOT           = "Publish ballot";
  private static final String PHASE_VOTER_CHECK_VERIFICATION_CODES = "Check verification codes (client side)";
  private static final String PHASE_VOTER_PUBLISH_CONFIRMATION     = "Publish confirmation";
  private static final String PHASE_VOTER_CHECK_FINALIZATION_CODES = "Check finalization code (client side)";

  private ApplicationArguments args;

  private LocalDateTime startTime;

  private Stopwatch stopwatch;

  private Map<FileType, List<Path>> pathsByFileType;

  private Map<String, Stopwatch>          simpleStopwatchByPhase;
  private Map<String, Map<Integer, Long>> durationByVoterByPhase;

  private long schemaSize;

  public TestResult(ApplicationArguments args) {
    this.args = args;
    pathsByFileType = new ConcurrentHashMap<>();
    simpleStopwatchByPhase = Collections.synchronizedMap(new LinkedHashMap<>()); // keep phases in added order
    durationByVoterByPhase = Collections.synchronizedMap(new LinkedHashMap<>()); // keep phases in added order
  }

  public ApplicationArguments getArgs() {
    return args;
  }

  public LocalDateTime getStartTime() {
    return startTime;
  }

  public Stopwatch getStopwatch() {
    return stopwatch;
  }

  public Map<String, Stopwatch> getSimpleStopwatchByPhase() {
    return simpleStopwatchByPhase;
  }

  public Map<String, Map<Integer, Long>> getDurationByVoterByPhase() {
    return durationByVoterByPhase;
  }

  /**
   * Marks the test as globally started.
   */
  public void testStarted() {
    startTime = LocalDateTime.now();
    stopwatch = Stopwatch.createStarted();
  }

  /**
   * Marks the test as globally finished.
   */
  public void testTerminated() {
    stopwatch.stop();
  }

  /**
   * Marks one phase of the test as started.
   *
   * @param phaseName a name for the phase
   */
  public void phaseStarted(String phaseName) {
    simpleStopwatchByPhase.put(phaseName, Stopwatch.createStarted());
  }

  /**
   * Marks one phase of the test as finished.
   *
   * @param phaseName phase name. Expected to be the same value as in a previous call to {@link #phaseStarted(String
   *                  phaseName)}.
   */
  public void phaseTerminated(String phaseName) {
    Stopwatch phaseStopwatch = simpleStopwatchByPhase.get(phaseName);
    phaseStopwatch.stop();
    if (log.isInfoEnabled()) {
      log.info("Phase {} ran in {}", phaseName,
               DurationFormatUtils.formatDurationWords(phaseStopwatch.elapsed(TimeUnit.MILLISECONDS), true, false));
    }
  }

  /**
   * @param voterStats
   */
  public void addVoterStats(Map<Integer, VotePerformanceStats> voterStats) {
    durationByVoterByPhase.put(PHASE_VOTER_PREPARE_BALLOT,
                               extractValues(voterStats, VotePerformanceStats::getBallotPreparationTime));
    durationByVoterByPhase.put(PHASE_VOTER_PUBLISH_BALLOT,
                               extractValues(voterStats, VotePerformanceStats::getBallotPublicationTime));
    durationByVoterByPhase.put(PHASE_VOTER_CHECK_VERIFICATION_CODES,
                               extractValues(voterStats, VotePerformanceStats::getCodesVerificationTime));
    durationByVoterByPhase.put(PHASE_VOTER_PUBLISH_CONFIRMATION,
                               extractValues(voterStats, VotePerformanceStats::getConfirmationPublicationTime));
    durationByVoterByPhase.put(PHASE_VOTER_CHECK_FINALIZATION_CODES,
                               extractValues(voterStats, VotePerformanceStats::getFinalizationVerificationTime));
  }

  private Map<Integer, Long> extractValues(Map<Integer, VotePerformanceStats> originalMap,
                                           Function<VotePerformanceStats, Long> extractor) {
    return originalMap.entrySet().stream()
                      .collect(Collectors.toMap(
                          Map.Entry::getKey,
                          entry -> extractor.apply(entry.getValue())));
  }

  /**
   * Adds a file to be displayed in the report.
   *
   * @param fileType file type
   * @param filePath path to the file
   */
  public void addFileToReport(FileType fileType, Path filePath) {
    pathsByFileType.computeIfAbsent(fileType, type -> new CopyOnWriteArrayList<>())
                   .add(filePath);
  }

  /**
   * Gets the data needed to report on the files generated by the test
   *
   * @return the paths of the files to be reported, grouped by file type
   */
  public Map<FileType, List<Path>> getPathsByFileType() {
    return pathsByFileType;
  }

  /**
   * Records the size of the schema after the test is executed
   *
   * @param size schema size in bytes
   */
  public void recordSchemaSize(long size) {
    schemaSize = size;
  }

  /**
   * Gets the schema size after the execution of the test.
   *
   * @return the size in bytes
   */
  public long getSchemaSize() {
    return schemaSize;
  }

}
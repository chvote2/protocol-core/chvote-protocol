/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.injector

import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDateTime
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class SharedFolderManagerTest extends Specification {

  @Rule
  TemporaryFolder temporaryFolder = new TemporaryFolder()

  Path rootFolder
  SharedFolderManager manager

  void setup() {
    rootFolder = temporaryFolder.newFolder().toPath()
    manager = new SharedFolderManager(rootFolder)
  }

  def "should be able to read all protocolIds"() {
    given:
    def protocolIds = ["pcol_12", "pcol_34", "pcol_56", "pcol_78"] as Set
    def creationDate = LocalDateTime.now()
    protocolIds.each {
      Files.createDirectories(rootFolder.resolve("protocol").resolve(it))
    }

    when:
    def result = manager.findDeclaredProtocolIdentifiers()

    then:
    result.keySet() == protocolIds
    result.values().every { it == creationDate || it.isAfter(creationDate) }
  }

  def "findDeclaredProtocolIdentifiers should fail if the expected directory does not exist"() {
    when:
    manager.findDeclaredProtocolIdentifiers()

    then:
    def ex = thrown UncheckedIOException
    ex.message =~ '^java\\.nio\\.file\\.NoSuchFileException: (.*)protocol$'
  }

  def "should point to the right epf folder"() {
    given:
    def protocolIds = ["pcol_12", "pcol_34", "pcol_56", "pcol_78"] as Set
    protocolIds.each {
      def folder = rootFolder.resolve("protocol").resolve(it)
      Files.createDirectories(folder)
      ["one/two", "printerFiles/printer-archive-pA_1", "printerFiles/printer-archive-pA_2", "other"].collect(folder.&resolve).each(Files.&createDirectories)
    }

    when:
    def folder_12_p1 = manager.getPrivateCredentialsLocation("pcol_12", "pA_1")
    def folder_12_p2 = manager.getPrivateCredentialsLocation("pcol_12", "pA_2")
    def folder_56_p1 = manager.getPrivateCredentialsLocation("pcol_56", "pA_1")

    then:
    folder_12_p1 == rootFolder.resolve("protocol/pcol_12/printerFiles/printer-archive-pA_1")
    folder_12_p2 == rootFolder.resolve("protocol/pcol_12/printerFiles/printer-archive-pA_2")
    folder_56_p1 == rootFolder.resolve("protocol/pcol_56/printerFiles/printer-archive-pA_1")
  }

  def "should fail if the epf folder does not exist"() {
    given:
    Files.createDirectories(rootFolder.resolve("protocol/pcol_12/printerFiles/printer-archive-abcd"))

    when:
    manager.getPrivateCredentialsLocation("pcol_12", "boh-vprt")

    then:
    def ex = thrown IllegalArgumentException
    ex.message == 'No directory found for printer "boh-vprt" and protocolId = pcol_12'
  }
}

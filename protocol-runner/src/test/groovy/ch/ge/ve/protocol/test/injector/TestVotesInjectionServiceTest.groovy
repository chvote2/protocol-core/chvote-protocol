/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.injector

import ch.ge.ve.protocol.core.model.IdentificationPrivateKey
import ch.ge.ve.protocol.model.IdentificationGroup
import ch.ge.ve.protocol.model.Tally
import ch.ge.ve.protocol.test.GeneratedVotesData
import ch.ge.ve.protocol.test.ProtocolDriver
import ch.ge.ve.protocol.test.ProtocolDriverFactory
import java.nio.file.Path
import spock.lang.Specification

class TestVotesInjectionServiceTest extends Specification {

  ProtocolDriverFactory protocolDriverFactory = Mock(ProtocolDriverFactory)

  SharedFolderManager sharedFolderManager = Mock(SharedFolderManager)

  IdentificationPrivateKey printerDecryptionKey = new IdentificationPrivateKey(5, new IdentificationGroup(3, 2, 2))

  TestVotesInjectionService service = new TestVotesInjectionService(
          protocolDriverFactory, printerDecryptionKey, sharedFolderManager,1, 2)

  def "list should delegate to the SharedFolderManager"() {
    given:
    def sharedFolderResult = [:]
    sharedFolderManager.findDeclaredProtocolIdentifiers() >> sharedFolderResult

    when:
    def result = service.listExistingProtocolIds()

    then:
    result.is( sharedFolderResult )
  }

  def "CastRandomVotes should command a ProtocolDriver to cast them"() {
    given:
    def protocolId = "pcol_test"
    def epfLocation = Mock(Path)
    def driver = Mock(ProtocolDriver)
    def generatedTally = new Tally([:])
    def votesData = new GeneratedVotesData([:], generatedTally)

    and:
    sharedFolderManager.getPrivateCredentialsLocation(protocolId, "boh-vprt") >> epfLocation

    when:
    def expectedTally = service.castRandomVotes(protocolId, 19)

    then:
    1 * protocolDriverFactory.retrieveAsReadyToCastVotes(protocolId, 1, 2) >> driver
    1 * driver.readVotersPrivateCredentials(epfLocation, ["boh-vprt": printerDecryptionKey])
    1 * driver.castVotes(19, epfLocation) >> votesData

    expectedTally.is( generatedTally )
  }
}

/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client

import ch.ge.ve.config.BigIntegerAsBase64Deserializer
import ch.ge.ve.config.BigIntegerAsBase64Serializer
import ch.ge.ve.event.ControlComponentPublicKeyMulticastEvent
import ch.ge.ve.event.ControlComponentPublicKeyPublicationEvent
import ch.ge.ve.event.Event
import ch.ge.ve.event.KeyGenerationRequestEvent
import ch.ge.ve.event.PublicParametersMulticastEvent
import ch.ge.ve.event.PublicParametersPublicationEvent
import ch.ge.ve.protocol.client.message.MessageHandlerCountMessagesImpl
import ch.ge.ve.protocol.client.progress.LoggingProgressTracker
import ch.ge.ve.protocol.client.progress.api.ProgressTracker
import ch.ge.ve.protocol.client.utils.RabbitUtilities
import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.support.PublicParametersFactory
import ch.ge.ve.service.Channels
import ch.ge.ve.service.Endpoints
import ch.ge.ve.service.EventHeaders
import ch.ge.ve.service.SignatureService
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import java.security.Security
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageProperties
import org.springframework.amqp.rabbit.listener.MessageListenerContainer
import org.springframework.amqp.support.converter.MessageConverter
import spock.lang.Specification

class ProtocolClientTest extends Specification {
  static final SECURITY_LEVEL = 1
  static final PROTOCOL_ID = "test-protocol"
  static final NBR_CONTROL_COMPONENTS = 2
  static final WAIT_TIMEOUT = 1000000L
  static final BATCH_SIZE = 25

  ProtocolClientImpl client
  RabbitUtilities rabbitUtilities
  SignatureService signatureService
  ObjectMapper objectMapper
  RandomGenerator randomGenerator
  ProgressTracker tracker
  MessageConverter messageConverter

  void setup() {
    Security.addProvider(new BouncyCastleProvider())
    objectMapper = createObjectMapper()
    rabbitUtilities = Mock(RabbitUtilities)
    signatureService = Mock(SignatureService)
    client = new ProtocolClientImpl(
            SECURITY_LEVEL,
            PROTOCOL_ID,
            NBR_CONTROL_COMPONENTS,
            rabbitUtilities,
            signatureService,
            objectMapper,
            randomGenerator,
            WAIT_TIMEOUT, BATCH_SIZE
    )
    tracker = new LoggingProgressTracker("protocol-client-test")
    messageConverter = Mock(MessageConverter)
  }


  def "should create and send the public parameters to the bulletin board"() {
    given: "a public parameters factory and a message handler"
    def publicParametersFactory = PublicParametersFactory.LEVEL_1
    def messageHandler = new MessageHandlerCountMessagesImpl<PublicParametersPublicationEvent>(
            PROTOCOL_ID,
            PublicParametersPublicationEvent.class,
            messageConverter,
            { message -> },
            WAIT_TIMEOUT
    )
    def messageListenerContainer = Mock(MessageListenerContainer)
    messageHandler.setContainer(messageListenerContainer)

    when: "the user sends the public parameters and the result is received"
    def publicParameters = client.sendPublicParameters(publicParametersFactory)
    messageHandler.handleMessage(createMockMessage(PublicParametersPublicationEvent.class))
    client.ensurePublicParametersForwardedToCCs(tracker)

    then: "the client creates a message handler"
    1 * rabbitUtilities.createDefaultMessageHandler(
            Channels.CONTROL_COMPONENT_INITIALIZATION,
            PROTOCOL_ID,
            PublicParametersMulticastEvent.class,
            false,
            WAIT_TIMEOUT) >> messageHandler

    and: "publishes the message to the bulletin board initialization message"
    1 * rabbitUtilities.publish(
            Channels.BULLETIN_BOARD_INITIALIZATION,
            PROTOCOL_ID,
            Endpoints.PROTOCOL_CLIENT,
            _ as Event)

    and: "stops the message listener container once the message has been ensured"
    1 * messageListenerContainer.stop()

    and: "returns the public parameters matching the expected security level and number of control components"
    publicParameters == publicParametersFactory.createPublicParameters(NBR_CONTROL_COMPONENTS)
  }

  def "should request the key generation and store it"() {
    given: "the publication and forwarding message handlers and listeners"
    def publicKeyPubEventHandler = new MessageHandlerCountMessagesImpl<ControlComponentPublicKeyPublicationEvent>(
            PROTOCOL_ID,
            ControlComponentPublicKeyPublicationEvent.class,
            messageConverter,
            { message -> },
            WAIT_TIMEOUT
    )
    def publicKeyForwardPubEventHandler = new MessageHandlerCountMessagesImpl<ControlComponentPublicKeyMulticastEvent>(
            PROTOCOL_ID,
            ControlComponentPublicKeyMulticastEvent.class,
            messageConverter,
            { message -> },
            WAIT_TIMEOUT
    )
    def messageListenerContainer = Mock(MessageListenerContainer)
    publicKeyPubEventHandler.setContainer(messageListenerContainer)
    publicKeyForwardPubEventHandler.setContainer(messageListenerContainer)

    when: "the user requests the key generation and the result is received"
    client.requestKeyGeneration()
    publicKeyPubEventHandler.handleMessage(createMockMessage(ControlComponentPublicKeyPublicationEvent.class))
    publicKeyPubEventHandler.handleMessage(createMockMessage(ControlComponentPublicKeyPublicationEvent.class))
    client.ensurePublicKeyPartsPublishedToBB(tracker)
    publicKeyForwardPubEventHandler.handleMessage(createMockMessage(ControlComponentPublicKeyMulticastEvent.class))
    publicKeyForwardPubEventHandler.handleMessage(createMockMessage(ControlComponentPublicKeyMulticastEvent.class))
    client.ensurePublicKeyPartsForwardedToCCs(tracker)

    then: "the client creates a message handler for the bulletin board publication"
    1 * rabbitUtilities.createDefaultMessageHandler(
            Channels.BULLETIN_BOARD_INITIALIZATION,
            PROTOCOL_ID,
            ControlComponentPublicKeyPublicationEvent.class,
            false,
            WAIT_TIMEOUT) >> publicKeyPubEventHandler
    and: "the client creates a message handler for the control component forwarding"
    1 * rabbitUtilities.createDefaultMessageHandler(
            Channels.CONTROL_COMPONENT_INITIALIZATION,
            PROTOCOL_ID,
            ControlComponentPublicKeyMulticastEvent.class,
            false,
            WAIT_TIMEOUT) >> publicKeyForwardPubEventHandler

    then: "the client requests the key generation"
    1 * rabbitUtilities.publish(
            Channels.CONTROL_COMPONENT_INITIALIZATION,
            PROTOCOL_ID,
            Endpoints.PROTOCOL_CLIENT,
            _ as KeyGenerationRequestEvent)

    and: "stops the message listener container for each message handler"
    2 * messageListenerContainer.stop()
  }

  def createMockMessage(Class<?> type) {
    def properties = new MessageProperties()
    properties.setHeader(EventHeaders.PROTOCOL_ID, PROTOCOL_ID)
    properties.setHeader("__TypeId__", type.getName())

    return new Message(new byte[0], properties)
  }

  def createObjectMapper() {
    def module = new SimpleModule()
    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer())
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())
    return new ObjectMapper().registerModule(module)
  }
}

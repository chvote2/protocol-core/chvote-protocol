/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.message;

import ch.ge.ve.event.Event;
import ch.ge.ve.protocol.client.message.api.MessageHandlerAbstractImpl;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import org.springframework.amqp.support.converter.MessageConverter;

/**
 * Handles messages of a given class only by counting them. Calling {@link #getEvents()} will always return
 * and empty list.
 *
 * @param <T> the message type.
 */
public final class MessageHandlerCountMessagesImpl<T  extends Event> extends MessageHandlerAbstractImpl<T> {
  private final Consumer<T>   messageConsumer;
  private final AtomicInteger messagesCount;

  public MessageHandlerCountMessagesImpl(String protocolId,
                                         Class<T> acceptedClass,
                                         MessageConverter converter,
                                         Consumer<T> messageConsumer,
                                         long waitTimeout) {
    super(waitTimeout, acceptedClass, protocolId, converter);
    this.messageConsumer = messageConsumer;
    this.messagesCount = new AtomicInteger(0);
  }

  @Override
  public void handleEvent(T event) {
    messageConsumer.accept(event);
    messagesCount.incrementAndGet();
  }

  /**
   * Returns an empty list.
   *
   * @return an empty list.
   */
  @Override
  public List<T> getEvents() {
    return Collections.emptyList();
  }

  @Override
  public int getMessagesCount() {
    return messagesCount.get();
  }

  @Override
  public void clear() {
    messagesCount.set(0);
  }

}
